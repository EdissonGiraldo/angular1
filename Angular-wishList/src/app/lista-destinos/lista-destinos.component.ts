import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos: String[];
  constructor() { 
    this.destinos = ['Bogota', 'Tocaima', 'Melgar', 'Montes de Maria', 'uribe Paraco Hijueputa']
  }

  ngOnInit(): void {
  }

}
